import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonDiscoverComponent } from './components/pokemon-discover/pokemon-discover.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetComponent } from './components/reset/reset.component';

const routes: Routes = [
  { path: 'reset', component: ResetComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'pokemon', component: PokemonListComponent },
  { path: 'discover', component: PokemonDiscoverComponent },
  { path: '**', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
