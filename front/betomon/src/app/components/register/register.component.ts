import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username: string = "";

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    let currentUser = this.userService.fetchLocalUser();
    console.log("currentUser : ", currentUser);

    if (currentUser) {
      this.router.navigate(['pokemon']);
    }
  }

  register() {
    this.userService.register(this.username).subscribe(() => {
      this.userService.setLocalUser(this.username);
      if (this.username !== '') {
        this.router.navigate(['pokemon']);
      }
    }, (error) => {
      if (error.error === "User already exist") {
        this.userService.setLocalUser(this.username);
        this.router.navigate(['pokemon']);
      }
    });

  }

}
