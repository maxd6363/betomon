import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-discover',
  templateUrl: './pokemon-discover.component.html',
  styleUrls: ['./pokemon-discover.component.css']
})
export class PokemonDiscoverComponent implements OnInit {
  pokemon: Pokemon = { name: "loading", imagePath: "", discovered: false };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(x => {
      let currentUsername = this.userService.fetchLocalUser();
      if (currentUsername && x['pokemon']) {
        this.pokemonService.discover(currentUsername, x['pokemon']).subscribe(pokemon => {
          this.pokemon = pokemon;
        });
      }
    });
  }

  back() {
    this.router.navigate(['pokemon']);
  }

  getPrettyName() {
    return this.pokemon.name.split('-')[0];
  }

}
