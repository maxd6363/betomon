import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  pokemons: Pokemon[] = [
    {
      imagePath: "https://i.ibb.co/FX6hym1/loading.gif",
      name: "Loading",
      discovered: true
    }];

  sad: Pokemon = {
    imagePath: "https://i.ibb.co/GVn7WKv/sad.gif",
    name: "Sad",
    discovered: true
  }

  unknown: string = "https://i.ibb.co/G2yRQLV/image-2022-08-09-233258909.png";
  constructor(private pokemonService: PokemonService, private userService: UserService) { }

  ngOnInit(): void {
    let currentUsername = this.userService.fetchLocalUser();
    if (currentUsername) {
      this.pokemonService.pokemons(currentUsername).subscribe(x => {
        this.pokemons = x;
      }, () => {
        this.pokemons = [this.sad];
      });
    }
    else {
      this.pokemons = [this.sad];
    }
  }
  numberPokemonDiscovered(): number {
    return this.pokemons.filter(x => x.discovered).length;
  }
}
