export class Pokemon {
  name: string;
  imagePath: string;
  discovered: boolean;

  constructor() {
    this.name = "";
    this.imagePath = "";
    this.discovered = false;
  }

}
