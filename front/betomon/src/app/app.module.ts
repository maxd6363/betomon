import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonDiscoverComponent } from './components/pokemon-discover/pokemon-discover.component';
import { FormsModule } from '@angular/forms';
import { ResetComponent } from './components/reset/reset.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    PokemonListComponent,
    PokemonDiscoverComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
