import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';
import { Pokemon } from '../models/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  discover(username: string, pokemon: string): Observable<Pokemon> {
    return this.http.post<Pokemon>(`${Constants.api_path}/pokemon/discover?user=${username}&pokemon=${pokemon}`, null);
  }

  pokemons(username: string): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`${Constants.api_path}/pokemon/get?user=${username}`);
  }

}
