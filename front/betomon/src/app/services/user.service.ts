import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  currentUsername: string = "";

  constructor(private http: HttpClient) {
  }

  register(name: string): Observable<any> {
    return this.http.post(`${Constants.api_path}/user/add?name=${name}`, null);
  }

  users(): Observable<User[]> {
    return this.http.get<User[]>(`${Constants.api_path}/users/get`);
  }

  fetchLocalUser(): string | null {
    return localStorage.getItem("username");
  }

  setLocalUser(username: string) {
    localStorage.setItem("username", username);
    this.currentUsername = username;
  }


  resetLocalUser() {
    localStorage.removeItem("username");
    this.currentUsername = "";
  }
}
