﻿namespace BetoMonAPI.Model
{
    public class Pokemon
    {
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool Discovered { get; set; }

        public Pokemon() { }

        public Pokemon(string name, string imagePaht = "", bool discovered = false)
        {
            Name = name;
            ImagePath = imagePaht;
            Discovered = discovered;
        }

        public Pokemon(Pokemon other) : this(other.Name, other.ImagePath, other.Discovered){}
    }
}
