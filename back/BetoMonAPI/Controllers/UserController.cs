﻿using BetoMonAPI.Model;
using BetoMonAPI.Store;
using Microsoft.AspNetCore.Mvc;

namespace BetoMonAPI.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {

        [HttpGet]
        [Route("api/users/get")]
        public IActionResult GetUsers()
        {
            return Ok(PokemonStore.Current.Users());
        }

        [HttpPost]
        [Route("api/user/add")]
        public IActionResult GetUsers(string name)
        {
            return PokemonStore.Current.AddUser(new User(name)) ? Ok() : BadRequest("User already exist");
        }





    }
}
