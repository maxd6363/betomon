﻿using BetoMonAPI.Model;
using BetoMonAPI.Store;
using Microsoft.AspNetCore.Mvc;

namespace BetoMonAPI.Controllers
{
    [ApiController]
    public class PokemonController : ControllerBase
    {

        [HttpGet]
        [Route("api/pokemon/get")]
        public IActionResult GetPokemon(string user)
        {
            var pokemons = PokemonStore.Current.GetPokemonsOfUser(new(user));
            return pokemons is null ? BadRequest("User not registered") : Ok(pokemons.ToList());
        }

        [HttpPost]
        [Route("api/pokemon/discover")]
        public IActionResult DiscoverPokemon(string user, string pokemon)
        {
            var pokemonFound = PokemonStore.Current.DiscoverPokemon(new(user), new(pokemon));
            return pokemonFound != null ? Ok(pokemonFound) : BadRequest("An error occurred");
        }

    }
}
