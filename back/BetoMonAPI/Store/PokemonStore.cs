﻿using BetoMonAPI.Model;
using Newtonsoft.Json;

namespace BetoMonAPI.Store
{
    public class PokemonStore
    {
        public static PokemonStore Current
        {
            get { return InternalPokemonStore ??= new(); }
        }

        private Dictionary<User, IList<Pokemon>> Store { get; }
        private IList<Pokemon> Pokemons { get; }
        private static PokemonStore? InternalPokemonStore { get; set; }


        private PokemonStore()
        {
            Store = new Dictionary<User, IList<Pokemon>>();
            string pokemonsJson = File.ReadAllText("Data/pokemons.json");
            Pokemons = JsonConvert.DeserializeObject<List<Pokemon>>(pokemonsJson) ?? new List<Pokemon>();
        }

        public bool AddUser(User user)
        {
            if (IsUserRegistered(user)) return false;
            Store.Add(user, Pokemons.Select(x => new Pokemon(x)).ToList());
            return true;
        }

        public bool IsUserRegistered(User user)
        {
            return Store.Keys.FirstOrDefault(x => x.Name == user.Name) != null;
        }

        public IList<User> Users()
        {
            return Store.Keys.ToList();
        }

        public IList<Pokemon>? GetPokemonsOfUser(User user)
        {
            if (!IsUserRegistered(user)) return null;
            return Store.FirstOrDefault(x => x.Key.Name == user.Name).Value;
        }

        public Pokemon? DiscoverPokemon(User user, Pokemon pokemon)
        {
            if (!IsUserRegistered(user)) return null;
            
            var pokemons = GetPokemonsOfUser(user);

            var pokemonFound = pokemons?.FirstOrDefault(x => x.Name == pokemon.Name);
            if(pokemonFound is null) return null;

            pokemonFound.Discovered = true;
            return pokemonFound;
        }

    }
}
