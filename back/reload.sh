echo $(tput setaf 1)Stopping server
sudo systemctl stop betomon

echo $(tput setaf 2)Building server
tput sgr0
sudo dotnet publish -c Release -o /srv/betomon/

sudo systemctl start betomon
echo $(tput setaf 2)Starting server

journalctl -u betomon.service -ef

